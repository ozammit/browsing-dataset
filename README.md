# Generate Keyword Datasets TEST

The aim of this project is to generate various datasets to be used in other project or studies. The data was collected from various students while they where searching online. A Google Chrome plugin was created to collect and send data to a central server. For each student the server created a data file containing all the search activity. The dataset is made up of the following attributes:

| Name | Description | Type |
|:-------------------|:-------------------------------------------------|:-----------|
| hash | A GUID that uniquely identifies the row. It can be used a s a primary key. | GUID |
| student | The student ID that searched the keyword. | GUID |
| call_time | The date and time when the search action was performed. | DateTime |
| url | Full URL visited by the user. | String |
| domain | The website domain where the user searched the keyword. | String |
| keyword | The keyword that was searched by the student. | String |

If you want to contribute to this dataset and benefit from an applictaion that will help you find relevant information online, download the applictaion and get started here https://bitbucket.org/ozammit/student-research-application.

To clone to this project use:

~~~
git clone https://ozammit@bitbucket.org/ozammit/browsing-dataset.git
~~~
